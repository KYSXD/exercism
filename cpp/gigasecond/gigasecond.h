#if !defined(GIGASECOND)
#define GIGASECOND

#include "boost/date_time/posix_time/posix_time.hpp"

namespace gigasecond {
    using boost::posix_time::seconds;
    using boost::posix_time::ptime;

    ptime advance(const ptime &actual) {
        return actual + seconds(long(1e9));
    }

}

#endif
