#if !defined(SPACE_AGE)
#define SPACE_AGE

#include <functional>
#include <string>
#include <unordered_map>

namespace space_age {
    class space_age;

    typedef std::function<double()> age_func;

    long EARTH_SPY = 31557600;  // Seconds Per Year

    std::unordered_map<std::string, double> WORLDS_RATIO = {

        {"mercury", 0.2408467},
        {"venus", 0.61519726},
        {"earth", 1.0},
        {"mars", 1.8808158},
        {"jupiter", 11.862615},
        {"saturn", 29.447498},
        {"uranus", 84.016846},
        {"neptune", 164.79132}

    };

}

class space_age::space_age {

private:
    long m_seconds;

    age_func convertion(const std::string &world) {
        double world_spy = EARTH_SPY * WORLDS_RATIO[world];
        return [=] () { return ( m_seconds / world_spy); };
    }

public:
    space_age(const long &seconds) : m_seconds(seconds) {}

    long seconds() const {
        return m_seconds;
    }

    age_func const on_mercury = convertion("mercury");
    age_func const on_venus = convertion("venus");
    age_func const on_earth = convertion("earth");
    age_func const on_mars = convertion("mars");
    age_func const on_jupiter = convertion("jupiter");
    age_func const on_saturn = convertion("saturn");
    age_func const on_uranus = convertion("uranus");
    age_func const on_neptune = convertion("neptune");

};

#endif
