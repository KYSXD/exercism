data_map = {
    'G': 'C',
    'C': 'G',
    'T': 'A',
    'A': 'U',
}


def to_rna(dna_strand):
    return ''.join(data_map[key] for key in dna_strand)
