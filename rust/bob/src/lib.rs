use regex::Regex;

const RES_QUESTION: &str = "Sure.";
const RES_YELL: &str = "Whoa, chill out!";
const RES_YELL_QUESTION: &str = "Calm down, I know what I'm doing!";
const RES_SILENCE: &str = "Fine. Be that way!";
const RES: &str = "Whatever.";

pub fn reply(message: &str) -> &str {
    // TODO: could regex be global static?
    let is_question = Regex::new(r"\?\s*\z").unwrap().is_match(message);
    let is_yell =
        Regex::new(r"[[:alpha:]]").unwrap().is_match(message) && message == message.to_uppercase();
    let is_silence = !Regex::new(r"\S").unwrap().is_match(message);

    match (is_question, is_yell, is_silence) {
        (true, true, _) => RES_YELL_QUESTION,
        (true, _, _) => RES_QUESTION,
        (_, true, _) => RES_YELL,
        (_, _, true) => RES_SILENCE,
        _ => RES,
    }
}
