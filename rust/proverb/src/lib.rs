pub fn build_proverb(list: &[&str]) -> String {
    let mut verses = vec![];

    let mut iterator = list.iter().peekable();
    while let Some(current) = iterator.next() {
        match iterator.peek() {
            Some(next) => verses.push(format!("For want of a {} the {} was lost.", current, next)),
            // TODO: Can I use something the iterator and not the list itself?
            None => verses.push(format!("And all for the want of a {}.", list[0])),
        }
    }

    verses.join("\n")
}
