pub fn factors(n: u64) -> Vec<u64> {
    let mut factors = vec![];
    let mut value = n;

    let ceil = (n as f64).sqrt() as u64;
    for i in 2..=ceil {
        while value % i == 0 {
            factors.push(i);
            value /= i;
        }
    }

    if value != 1 {
        factors.push(value);
    }

    factors
}
