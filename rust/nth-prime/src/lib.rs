pub fn nth(n: u32) -> u32 {
    let mut index = 2;
    let mut primes = 0;

    while primes < n {
        index += 1;
        if is_prime(index) {
            primes += 1;
        }
    }

    return index;
}

pub fn is_prime(n: u32) -> bool {
    if n == 2 {
        return true;
    } else if n % 2 == 0 {
        return false;
    }

    for i in (3..=(n as f32).sqrt() as u32).step_by(2) {
        if n % i == 0 {
            return false;
        }
    }

    return n > 1;
}
