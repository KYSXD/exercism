pub fn verse(n: i32) -> String {
    let first_sentence = format!(
        "{} on the wall, {}.",
        bottles_of_beer_string(n, true),
        bottles_of_beer_string(n, false),
    );

    let second_sentence = format!(
        "{}, {} on the wall.",
        action(n),
        bottles_of_beer_string((n + 99) % 100, false)
    );

    first_sentence + "\n" + &second_sentence + "\n"
}

pub fn sing(start: i32, end: i32) -> String {
    let mut verses = vec![];

    for i in (end..=start).rev() {
        verses.push(verse(i));

        if i != end {
            verses.push("\n".to_string());
        }
    }

    verses.concat()
}

pub fn bottles_of_beer_string(n: i32, capital: bool) -> String {
    let amount = match n {
        0 => format!("{}o more bottles", if capital { "N" } else { "n" }),
        _ => format!("{} bottle{}", n, if n > 1 { "s" } else { "" }),
    };

    amount + " of beer"
}

pub fn action(n: i32) -> String {
    match n {
        0 => "Go to the store and buy some more".to_string(),
        _ => format!(
            "Take {} down and pass it around",
            if n == 1 { "it" } else { "one" }
        ),
    }
}
