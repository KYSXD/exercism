pub fn is_leap_year(year: u64) -> bool {
    // Is this be faster?
    // (year & 3 == 0) && (year % 25 != 0 || year & 15 == 0)
    year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)
}
