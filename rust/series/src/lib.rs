pub fn series(digits: &str, len: usize) -> Vec<String> {
    match len {
        // TODO: is it possible to work all cases in one loop?
        0 => vec!["".to_string(); digits.len() + 1],
        _ => digits
            .chars()
            .collect::<Vec<char>>()
            .windows(len)
            .map(|substr_iter| substr_iter.iter().collect::<String>())
            .collect(),
    }
}
