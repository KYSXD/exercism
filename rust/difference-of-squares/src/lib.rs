pub fn square_of_sum(n: u32) -> u32 {
    // (1+2+3+..+n)^2 = ((n)(n+1)/2)^2
    (n * n + n) * (n * n + n) / 4
}

pub fn sum_of_squares(n: u32) -> u32 {
    // 1+4+9+16+...+n^2 = (n)(n+1)(2n+1)(1/6)
    n * (n + 1) * (n + n + 1) / 6
}

pub fn difference(n: u32) -> u32 {
    square_of_sum(n) - sum_of_squares(n)
}
