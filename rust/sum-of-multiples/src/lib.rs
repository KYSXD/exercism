pub fn sum_of_multiples(limit: u32, factors: &[u32]) -> u32 {
    (1..limit)
        .filter(|&m| factors.iter().any(|&d| d > 0 && m % d == 0))
        .sum()
}
